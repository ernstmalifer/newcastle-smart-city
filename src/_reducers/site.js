const initialState = {
  debug: false,
  endpoint: window.endpoint,
  subscriberID: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
};