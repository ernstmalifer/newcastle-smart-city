/* eslint-disable no-undef */
import React, { Component } from 'react';

import { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  OverlayView
} from "react-google-maps";

const getPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height / 2),
})

class Map extends Component {
  render() {
    return (
      <div id="map-home">
        <GoogleMap defaultZoom={17.5} defaultCenter={new google.maps.LatLng(-32.927762, 151.772873)}>
          <Marker position={{ lat: -32.927762, lng: 151.772873 }}/>
          <OverlayView position={{ lat: -32.927762, lng: 151.772873 }} mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET} getPixelPositionOffset={getPixelPositionOffset}>
            <div id="youarehere">
              <i className="fa fa-male"></i>
              You Are Here<br/>
              <span>Newcastle City Council</span>
            </div>
          </OverlayView>
        </GoogleMap>
      </div>
    )
  }
}

export default compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDIKR0yRvzJXK6HH75UK631NG_9S4cdXio",
    loadingElement: <div style={{ height: `400px` }}/>,
    containerElement: <div id="map-home" style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `400px` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(Map);

