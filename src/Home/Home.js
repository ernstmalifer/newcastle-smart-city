/* eslint-disable no-undef */
import React, { Component } from 'react';
import './Home.css';

import Map from './Map';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import { Link } from 'react-router-dom';

class Home extends Component {

  state = {
  }

  componentDidMount = () => {
    this.slideshowInterval = setInterval(() => {
      this.slideshow.slickNext();
    }, 5000);
  }

  componentWillUnmount = () => {
    clearInterval(this.slideshowInterval);
  }

  render() {

    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      customPaging: (dots) => {
        return <React.Fragment>{dots}</React.Fragment>
      }
    };

    return (
      <div id="home">
        <div id="slideshow">
        <Slider {...settings} ref={c => this.slideshow = c}>
          <div>
            <img alt="" src="/images/slides/slide-01.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-02.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-03.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-04.jpg" />
          </div>
        </Slider>
        </div>

        <div id="controls">
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <Link to="/wayfinding">
                  <div className="control" id="control-wayfinding">
                    <i className="fa fa-map-marked-alt"></i><br/>
                    Wayfinding
                  </div>
                </Link>
              </div>
              <div className="col">
                <Link to="/whatson">
                  <div className="control" id="control-whatson">
                    <i className="fa fa-calendar-alt"></i><br/>
                    What's On
                  </div>
                </Link>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <Link to="/localhistory" >
                  <div className="control" id="control-history">
                    <i className="fa fa-book"></i><br/>
                    Local History
                  </div>
                </Link>
              </div>
              <div className="col">
                <Link to="/touristinfo">
                  <div className="control" id="control-touristinfo">
                    <i className="fa fa-id-card"></i><br/>
                    Tourist Info
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <Map />

      </div>
    )
  }
}

export default Home;

