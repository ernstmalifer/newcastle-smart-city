/* eslint-disable no-undef */
import React, { Component } from 'react';
import './Whatson.css';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import { Link } from 'react-router-dom';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

// import Events from './events.json';

import moment from 'moment';

import { createPortal } from 'react-dom';
import EventModal from './EventModal';

class Whatson extends Component {

  state = {
    Events: [],
    schedule: 'all',
    sortedEvents: {
      all: [],
      today: [],
      tomorrow: [],
      nextweek: []
    },
    showEventItem: true,
  }

  componentDidMount = () => {
    this.slideshowInterval = setInterval(() => {
      this.slideshow.slickNext();
    }, 5000);

    fetch('/events.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({Events: resp.events}, () => {
        this.sortEventsBySchedule();
      });
    });
  }

  componentWillUnmount = () => {
    clearInterval(this.slideshowInterval);
  }

  sortEventsBySchedule = () => {

    let sortedEvents = {
      all: [],
      today: [],
      tomorrow: [],
      nextweek: [],
      showEventModal: false,
      event: null,
    }

    this.state.Events.forEach(event => {
      sortedEvents.all.push(event);
      if(moment(event.event_date_start).isBetween(moment().startOf('day'), moment().endOf('day'))){
        sortedEvents.today.push(event);
      } else if(moment(event.event_date_start).isBetween(moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day'))){
        sortedEvents.tomorrow.push(event);
      } else if(moment(event.event_date_start).isBetween(moment().add(1, 'weeks').startOf('isoWeek'), moment().add(1, 'weeks').endOf('isoWeek'))){
        sortedEvents.nextweek.push(event);
      } else {
      }
    });

    this.setState({sortedEvents});
  }

  setSchedule = (schedule) => {
    if(this.state.schedule === schedule) {
      this.setState({schedule: 'all', showEventItem: true});
    } else {
      this.setState({schedule, showEventItem: true});
    }
  }

  showEventModal = (show, event) => {
    this.setState({showEventModal: show, event});
  }

  render() {

    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      customPaging: (dots) => {
        return <React.Fragment>{dots}</React.Fragment>
      }
    };

    return (
      <div id="whatson">
        <div id="slideshow">
        <Slider {...settings} ref={c => this.slideshow = c}>
          <div>
            <img alt="" src="/images/slides/slide-01.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-02.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-03.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-04.jpg" />
          </div>
        </Slider>
        </div>

        <div id="whatson-controls">
          <Link to="/" className="back-button">
            <i className="fa fa-chevron-left"></i>Home
          </Link>
        </div>

        <div id="whatson-schedules">
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h1 className="page-title">What's Happening...</h1>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <button className={`btn btn-schedule btn-light btn-hg ${this.state.schedule === 'today' ? `active` : ``}`} onClick={() => { this.setSchedule('today') }}>Today</button>
                <button className={`btn btn-schedule btn-light btn-hg ${this.state.schedule === 'tomorrow' ? `active` : ``}`} onClick={() => { this.setSchedule('tomorrow') }}>Tomorrow</button>
                <button className={`btn btn-schedule btn-light btn-hg ${this.state.schedule === 'nextweek' ? `active` : ``}`} onClick={() => { this.setSchedule('nextweek') }}>Next Week</button>
              </div>
            </div>
          </div>
        </div>

        <div id="whatson-events">
          <TransitionGroup className="events-container-container">
            {Object.keys(this.state.sortedEvents).map((key) => {
              if(this.state.schedule === key){
                return (
                  <CSSTransition timeout={500} classNames="slide" key={`container-${key}`} >
                  <div id={`container-${key}`}  className="events-container-group">
                    <TransitionGroup>
                      {this.state.sortedEvents[key].map((event, index) => {
                        
                        let delay = Math.max(0, index * 100);

                        return (
                          <CSSTransition key={event.id} timeout={1000 + delay} classNames="slideUp" style={{transition: `transform 300ms linear ${delay}ms`}}>
                          <a className={`btn card-event`}  onClick={() => { this.showEventModal(true, event) }}>
                            <div>
                            <img className="card-img-left" alt="" style={{backgroundImage: `url("${event.event_images[0]}")`}} />
                            <div className="card-container">
                              <h5 className="card-title">{event.event_name}</h5>
                              <p className="card-text">{event.event_description_excerpt}</p>
                              {moment(event.event_date_start).isValid() && moment(event.event_date_end).isValid() &&
                                <React.Fragment>
                                  <p className="card-date">{moment(event.event_date_start).format('ddd D MMM')} - {moment(event.event_date_end).format('ddd D MMM')}, {moment(event.event_date_start).format('H:mm a')} -  {moment(event.event_date_end).format('H:mm a')}</p>
                                </React.Fragment>
                              }
                              {!moment(event.event_date_start).isValid() && !moment(event.event_date_end).isValid() &&
                                <React.Fragment>
                                   <p className="card-date">{event.event_date_start}</p>
                                </React.Fragment>
                              }
                              
                            </div>
                            </div>
                          </a>
                          </CSSTransition>
                        )

                      })}
                    </TransitionGroup>
                  </div>
                  </CSSTransition>
                )
              }
              return null;
            })}
          </TransitionGroup>
        </div>

        {this.state.showEventModal && 
          createPortal( <EventModal event={this.state.event} hideEventModal={ () => {this.showEventModal(false);}}></EventModal>, document.getElementById('modal-portal'))
        }

      </div>
    )
  }
}

export default Whatson;

