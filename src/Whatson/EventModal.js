import React, { Component } from 'react';

import moment from 'moment';

import QRCode from 'qrcode.react';

import Slider from "react-slick";

import './EventModal.css';

class EventModal extends Component {

  state = {
    willShow: false,
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({willShow: true})
    }, 100);
  }

  hideEventModal = () => {
    this.setState({willShow: false}, () => {
      setTimeout(() => {
        this.props.hideEventModal();
      }, 100);
    })
  }

  render() {

    const slideshowSettings = {
      customPaging: (i) => {
        return (
          <a>
            <img alt="" src={`${this.props.event.event_images[i]}`} />
          </a>
        );
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <React.Fragment>
        <div className={`modal fade ${this.state.willShow ?  `show` : ``}`} role="dialog" style={{display: this.state.willShow ? `block` : ``}}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="row">
                  <div className="col-12">
                    <div className="" style={{float: 'right', width: '300px', marginLeft: '30px'}}>
                      <Slider {...slideshowSettings}>
                        {this.props.event.event_images.map((image, index) => {
                          return (
                            <img key={`modal-event-image-${index}`} className="modal-event-image" alt="" src={`${image}`} />
                          )
                        })}
                      </Slider>
                      {/* <img className="modal-event-image" alt="" style={{backgroundImage: `url("${this.props.event.event_image}")`}} /> */}
                    </div>
                    <h5 className="modal-title event-modal-title">{this.props.event.event_name}</h5>
                    {moment(this.props.event.event_date_start).isValid() && moment(this.props.event.event_date_end).isValid() &&
                      <React.Fragment>
                        <p className="event-description-item"><i className="fa fa-calendar-alt"></i>{moment(this.props.event.event_date_start).format('Do MMMM YYYY, dddd')}</p>
                        <p className="event-description-item"><i className="fa fa-clock"></i>{moment(this.props.event.event_date_start).format('H:mm a')}-{moment(this.props.event.event_date_end).format('H:mm a')}</p>
                      </React.Fragment>
                    }
                    {!moment(this.props.event.event_date_start).isValid() && !moment(this.props.event.event_date_end).isValid() &&
                      <React.Fragment>
                        <p className="event-description-item"><i className="fa fa-calendar-alt"></i>{this.props.event.event_date_start}</p>
                      </React.Fragment>
                    }
                    {this.props.event.event_location && 
                      <p className="event-description-item"><i className="fa fa-map-marker-alt"></i>{this.props.event.event_location}</p>
                    }
                      <p className="event-description-item">&nbsp;</p>
                    {this.props.event.event_price && 
                      <p className="event-description-item"><i className="fa fa-dollar-sign"></i><span dangerouslySetInnerHTML={{__html: this.props.event.event_price}}></span></p>
                    }
                    {this.props.event.event_contact && 
                      <p className="event-description-item"><i className="fa fa-phone"></i>{this.props.event.event_contact}</p>
                    }
                    {this.props.event.event_url && 
                      <p className="event-description-item"><i className="fa fa-link"></i>{this.props.event.event_url}</p>
                    }
                    {this.props.event.event_email && 
                      <p className="event-description-item"><i className="fa fa-at"></i>{this.props.event.event_email}</p>
                    }
                    {this.props.event.event_organiser && 
                      <p className="event-description-item"><i className="fa fa-user-alt"></i>{this.props.event.event_organiser}</p>
                    }
                    
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="event-description" dangerouslySetInnerHTML={{__html: this.props.event.event_description}} />
                    <p className="event-qr">
                      <QRCode value={this.props.event.event_url} size={128} bgColor={"#ffffff"} fgColor={"#000000"} level={"L"}/>
                      <span className="qr-knowmore">Scan the QR code to know more!</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="event-close-btn btn btn-lg btn-secondary" data-dismiss="modal" onClick={() => {this.hideEventModal()}}>Close</button>
              </div>
            </div>
          </div>
        </div>
        <div className={`modal-backdrop fade ${this.state.willShow ?  `show` : `hide`}`}></div>
      </React.Fragment>
    );
  }
}

export default EventModal;