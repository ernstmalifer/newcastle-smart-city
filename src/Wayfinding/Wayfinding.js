/* eslint-disable no-undef */
import React, { Component } from 'react';
import './Wayfinding.css';

import { Link } from 'react-router-dom';

import { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker,
  OverlayView
} from "react-google-maps";

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import Keyboard from 'react-virtual-keyboard';


const getPixelPositionOffset = (width, height) => ({
  x: -(width / 1.5),
  y: -(height / 1.5),
})


class Wayfinding extends Component {

  state = {
    Categories: [],
    search: '',
    showSearchKeyboard: false,
    Destinations: [],
    tab: null,
    directions: null,
    activeDirectionID: 0,
    lineOffset: 0,
  }
  
  componentDidMount() {
    this.DirectionsService = new google.maps.DirectionsService();

    fetch('/categories.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({Categories: resp.categories}, () => {
        this.setState({tab: this.state.Categories[0].id})
      });
    });

    fetch('/destinations.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({Destinations: resp.destinations});

    });

    // this.incrementLineOffset = setInterval(() => {
    //   this.setState({lineOffset: this.state.lineOffset + 1});
    // }, 1000)
  }

  showSearchKeyboard = (e) => {
    this.setState({showSearchKeyboard: true});
  }

  hideSearchKeyboard = () => {
    this.setState({showSearchKeyboard: false});
  }

  onSearchChanged = (search) => {
    
    if(search[search.length - 1] === '✖') {
      this.setState({showSearchKeyboard: false});
    } else {
      this.setState({search})
    }
  }

  onSearchSubmitted = (search) => {
    this.setState({showSearchKeyboard: false});
  }

  changeTab = (tab) => {
    this.setState({tab});
  }

  changeDirection = (destinationID, destination) => {
    this.setState({activeDirectionID: destinationID, destination}, () => {
      this.DirectionsService.route({
          origin: 'Newcastle City Hall, 290 King St, Newcastle NSW 2300, Australia',
          destination: this.state.destination,
          travelMode: google.maps.TravelMode.DRIVING,
        }, (result, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            this.setState({
              directions: result,
            });
          } else {
            console.error(`error fetching directions ${result}`);
          }
        });
    });
  }

  componentWillMount = () => {
    // clearInterval(this.incrementLineOffset);
  }

  render() {

    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      strokeWeight: '10',
      scale: 4
    };

    return (
      <div id="wayfinding">
        <Link to="/" className="back-button">
          <i className="fa fa-chevron-left"></i>Home
        </Link>
        <div>
        <GoogleMap defaultZoom={19} defaultCenter={new google.maps.LatLng(-32.927762, 151.772873)} >
          {this.state.directions && <DirectionsRenderer directions={this.state.directions} options={{polylineOptions: {strokeOpacity: 0, strokeColor: '#00A5A5', icons: [{icon: lineSymbol, offset: '0', repeat: '30px'}]}}} />}
          <Marker position={{ lat: -32.927762, lng: 151.772873 }}/>
          <OverlayView position={{ lat: -32.927762, lng: 151.772873 }} mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET} getPixelPositionOffset={getPixelPositionOffset}>
            <div id="youarehere">
              <i className="fa fa-male"></i>
              You Are Here<br/>
              <span>Newcastle City Council</span>
            </div>
          </OverlayView>
        </GoogleMap>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="input-group search">
              <div className="input-group-prepend">
                <span className="input-group-text" id="validationTooltipUsernamePrepend">
                  <i className="fa fa-search"></i>
                </span>
              </div>
                <input className="form-control form-control-lg" type="text" placeholder="Enter Keyword" value={this.state.search} onClick={this.showSearchKeyboard} />
                {this.state.showSearchKeyboard &&
                  <React.Fragment>
                  <div className="keyboard-overlay" onTouchStart={() => {this.hideSearchKeyboard()}}></div>
                  <Keyboard value={this.state.search} name='keyboard' options={{ type:"input", alwaysOpen: true, usePreview: false, useWheel: false, stickyShift: false, appendLocally: true, color: "light", updateOnChange: true, initialFocus: true, display: { "accept" : "Submit", "enter": "Cancel" }, layout: 'custom',
                  customLayout: {
                    'normal': [
                      '` 1 2 3 4 5 6 7 8 9 0 - = {bksp} ✖',
                      'q w e r t y u i o p [ ]',
                      'a s d f g h j k l ; \' \\',
                      '{shift} z x c v b n m , . / {shift}',
                      '{space} {accept}'
                    ],
                    'shift': [
                      '~ ! @ # $ % ^ & * ( ) _ + {bksp} ✖',
                      'Q W E R T Y U I O P { }',
                      'A S D F G H J K L : " |',
                      '{shift} Z X C V B N M < > ? {shift}',
                      '{space} {accept}'
                    ]
                  } }} onChange={this.onSearchChanged} onAccepted={this.onSearchSubmitted} ref={k => this.keyboard = k}
                  ></Keyboard>
                  </React.Fragment>
                }
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="tab-group">
                {this.state.Categories.map((category) => {
                  return (
                    <button key={category.id} type="button" onClick={() => {this.changeTab(category.id)}} className={`btn btn-tab btn-link ${this.state.tab === category.id ? `active` : ``}`}><i className={`${category.category_icon}`}></i><span>{category.category_name}</span></button>
                  )
                })}
              </div>
            </div>
          </div>
          <TransitionGroup className="container-container">
          {this.state.Categories.map((category, idx) => {
            if(this.state.tab === category.id){
              return (
                <CSSTransition timeout={500} classNames="slide" key={`container-${category.id}`}>
                <div id={`container-${category.id}`}  className="container-group" onTouchStart={() => {this.hideSearchKeyboard()}}>
                    {this.state.Destinations.map((destination) => {
                      if(destination.destination_category === category.id) {
                        return (
                          <a key={destination.id} className={`btn card-destination ${this.state.activeDirectionID === destination.id ? `active` : ``}`}  onClick={() => {this.changeDirection(destination.id, destination.destination_location)}}>
                            <img className="card-img-left" alt="" style={{backgroundImage: `url("${destination.destination_image}")`}} />
                            <div className="card-container">
                              <h5 className="card-title">{destination.destination_name}</h5>
                              <p className="card-text">{destination.destination_location}</p>
                              {destination.id === '2e2599e0761246d7a157fd493d5eb6ae' &&
                                <i className="fa fa-male"></i>
                              }
                            </div>
                          </a>
                        )
                      }
                      return null;
                    })}
                </div>
                </CSSTransition>
              )
            }
            return null;
          })}
        </TransitionGroup>
        </div>
      </div>
    );
  }
}

export default compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDIKR0yRvzJXK6HH75UK631NG_9S4cdXio",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div />,
    mapElement: <div style={{ height: `1000px` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(Wayfinding);
