import React, { Component } from 'react';

import QRCode from 'qrcode.react';

import './LocalHistoryItemModal.css';

class LocalHistoryItemModal extends Component {

  state = {
    willShow: false,
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({willShow: true})
    }, 100);
  }

  hideLocalHistoryItemModal = () => {
    this.setState({willShow: false}, () => {
      setTimeout(() => {
        this.props.hideLocalHistoryItemModal();
      }, 100);
    })
  }

  render() {
    return (
      <React.Fragment>
        <div className={`modal fade ${this.state.willShow ?  `show` : ``}`} role="dialog" style={{display: this.state.willShow ? `block` : ``}}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="row">
                  <div className="col">
                    <img className="modal-local-history-item-image" alt="" style={{backgroundImage: `url("${this.props.localHistoryItem.image}")`}} />
                    <h5 className="modal-title local-history-item-modal-title">{this.props.localHistoryItem.name}</h5>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="local-history-item-description" dangerouslySetInnerHTML={{__html: this.props.localHistoryItem.description}} />
                    <p className="local-history-item-qr">
                      <QRCode value={this.props.localHistoryItem.url} size={128} bgColor={"#ffffff"} fgColor={"#000000"} level={"L"}/>
                      <span className="qr-knowmore">Scan the QR code to know more!</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="local-history-item-close-btn btn btn-lg btn-secondary" data-dismiss="modal" onClick={() => {this.hideLocalHistoryItemModal()}}>Close</button>
              </div>
            </div>
          </div>
        </div>
        <div className={`modal-backdrop fade ${this.state.willShow ?  `show` : `hide`}`}></div>
      </React.Fragment>
    );
  }
}

export default LocalHistoryItemModal;