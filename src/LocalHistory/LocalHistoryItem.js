/* eslint-disable no-undef */
import React, { Component } from 'react';
import './LocalHistoryItem.css';

import { Link } from 'react-router-dom';

import LocalHistoryItemModal from './LocalHistoryItemModal';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { createPortal } from 'react-dom';

class LocalHistoryItem extends Component {

  state = {
    Histories: [],
    LocalHistories: [],
    title: '',
    description: '',
    category: '',
    showLocalHistoryItemModal: false,
    localHistoryItem: null,
    showHistoryItems: false
  }

  // componentWillMount = () => {
  //   switch(this.props.match.params.item) {
  //     case 'exhibitionsandevents':
  //       this.setState({title: 'Exhibitions & Events', category: this.props.match.params.item});
  //       break;
  //     case 'familyhistory':
  //       this.setState({title: 'Family History', category: this.props.match.params.item});
  //       break;
  //     case 'localhistorylibrary':
  //       this.setState({title: 'Local History Library', category: this.props.match.params.item});
  //       break;
  //     case 'newcastlecollections':
  //       this.setState({title: 'Newcastle Collections', category: this.props.match.params.item});
  //       break;
  //     case 'searchthecollections':
  //       this.setState({title: 'Search The Collections', category: this.props.match.params.item});
  //       break;
  //     default:
  //       break;
  //   }
  // }

  componentDidMount = () => {
    this.setState({showHistoryItems: true});

    fetch('/histories.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({Histories: resp.histories}, () => {
        this.setTitleDescription();
      });
    });

    fetch('/localhistories.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({LocalHistories: resp.localhistories});
    });
  }

  setTitleDescription = () => {
    const historyItem = this.state.Histories.find((history_item) => {
      return history_item.id === this.props.match.params.item;
    });
    
    this.setState({title: historyItem.name, description: historyItem.description, category: this.props.match.params.item});
  }

  showLocalHistoryItemModal = (show, localHistoryItem) => {
    this.setState({showLocalHistoryItemModal: show, localHistoryItem});
  }

  componentWillUnmount = () => {
  }

  render() {

    return (
      <div id="localhistoryitem">

        <div id="localhistoryitem-controls">
          <Link to="/" className="back-button">
            <i className="fa fa-chevron-left"></i>Home
          </Link>
        </div>

      <div id="localhistoryitem-container">
        <div id="localhistoryitem-description">
          <div className="row">
            <div className="col" style={{flex: '0 0 140px'}}>
              <Link to="/localhistory" className="historyback-button"><i className="fa fa-chevron-left"></i></Link>
            </div>
            <div className="col">
              <h1>{this.state.title}</h1>
              <div className="localhistory-item-description" dangerouslySetInnerHTML={{__html: this.state.description}}></div>
            </div>
          </div>
        </div>

        <div id="localhistoryitems">
          <div className="row">
            <div className="col" style={{flex: '0 0 140px'}}>
            </div>
            <div className="col">
              <div className="localhistoryitemslist">
                <TransitionGroup>
                {this.state.LocalHistories.map((localhistory, index) => {
                  
                  let delay = Math.max(0, index * 100);

                  if(this.state.showHistoryItems && localhistory.category === this.state.category){
                    return (
                      <CSSTransition key={`history-${localhistory.id}`} classNames="slideUp" timeout={3000 + delay} style={{transitionDelay: `${delay}ms`}}>
                        <a key={localhistory.id} className={`btn card-localhistory`} onClick={() => { this.showLocalHistoryItemModal(true, localhistory) }}>
                          <img className="card-img-left" alt="" style={{backgroundImage: `url("${localhistory.image}")`}} />
                          <div className="card-container">
                            <h5 className="card-title">{localhistory.name}</h5>
                            <p className="card-text">{localhistory.description_excerpt}</p>
                          </div>
                        </a>
                      </CSSTransition>
                    )
                  }
                  return null;
                })}
                </TransitionGroup>
              </div>
            </div>
          </div>
        </div>

        </div>

        {this.state.showLocalHistoryItemModal && 
          createPortal( <LocalHistoryItemModal localHistoryItem={this.state.localHistoryItem} hideLocalHistoryItemModal={ () => {this.showLocalHistoryItemModal(false);}}></LocalHistoryItemModal>, document.getElementById('modal-portal'))
        }

      </div>
    )
  }
}

export default LocalHistoryItem;

