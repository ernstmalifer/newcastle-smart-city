/* eslint-disable no-undef */
import React, { Component } from 'react';
import './LocalHistory.css';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { Link } from 'react-router-dom';

class LocalHistory extends Component {

  state = {
    Histories: [],
    showHistories: false
  }

  componentDidMount = () => {
    this.setState({showHistories: true});
    fetch('/histories.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({Histories: resp.histories});
    });
  }

  componentWillUnmount = () => {
  }

  render() {

    return (
      <div id="localhistory">

        <div id="localhistory-controls">
          <Link to="/" className="back-button">
            <i className="fa fa-chevron-left"></i>Home
          </Link>
        </div>

        <div id="localhistory-description">
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h1>Local History</h1>
                <p>The Awabakal and Worimi peoples are the traditional custodians of the land and waters of Newcastle. After British settlement, Newcastle become a convict settlement and has a long heritage as an industrial and harbour city.</p>
                <p>Newcastle Region Library collects, preserves and provides access to a range of materials which document the history of Newcastle and the Hunter and its people. Whether you are a family historian, a student or interested in the heritage of the local area, Newcastle Region Library has the resources to help you.</p>
              </div>
            </div>
          </div>
        </div>

        <div id="localhistories">
          <TransitionGroup>
            {this.state.Histories.map((history_item, index) => {
              
              let delay = Math.max(0, index * 100);

              if(this.state.showHistories){
                return (
                  <CSSTransition key={`history-${history_item.id}`} timeout={1000 + delay} classNames="slideUp" style={{transitionDelay: `${delay}ms`}}>
                    <Link to={`/localhistory/${history_item.id}`} className={`btn card-localhistory`} >
                      <img className="card-img-left" alt="" style={{backgroundImage: `url("${history_item.image}")`}} />
                      <div className="card-container">
                        <h5 className="card-title">{history_item.name}</h5>
                        <p className="card-text">{history_item.description_excerpt}</p>
                      </div>
                    </Link>
                  </CSSTransition>
                )
              }
              return null;
            })}
            
          </TransitionGroup>
        </div>

      </div>
    )
  }
}

export default LocalHistory;

