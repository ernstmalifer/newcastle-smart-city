/* eslint-disable no-undef */
import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {

  render() {
    return (
      <div id="footer">
        <div className="row no-gutters">

          <div className="col-6">
            <img src="/images/logo.png" style={{margin: '20px'}} alt="Newcastle" width="150" />
          </div>

          <div className="col-6">
          </div>

        </div>
      </div>
    );
  }
}

export default Footer;
