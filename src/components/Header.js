/* eslint-disable no-undef */
import React, { Component } from 'react';
import './Header.css';

import moment from 'moment';

class Header extends Component {

  state = {
    datetime: moment()
  }

  componentDidMount = () => {
    this.tickTime = setInterval(() => {
      this.setState({datetime: moment()});
    }, 1000)
  }

  render() {
    return (
      <div id="header">
        <div className="row no-gutters">

          <div className="col-6 col-date-time">
            <div className="row text-center">
              <div className="col">
                <span className="header-date">{this.state.datetime.format(`D MMM YYYY`)}</span>
                <span className="header-time">{this.state.datetime.format(`ddd h:mm A`)}</span>
              </div>
            </div>
          </div>

          <div className="col-6 col-weather">
            <div className="text-center">
              <div className="col text-center">
                <span className="header-temperature">28&deg;</span>
                <i className="header-weather-icon wi wi-day-cloudy"></i>
                <span className="header-weather text-left">Partly Cloudy in <br/><span className="header-weather-location">282 King Street</span></span>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Header;
