/* eslint-disable no-undef */
import React, { Component } from 'react';
import './TouristInfo.css';

import { Link } from 'react-router-dom';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import TouristInfoModal from './TouristInfoModal';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { createPortal } from 'react-dom';

class TouristInfo extends Component {

  state = {
    TouristInfos: [],
    title: '',
    showTouristInfoModal: false,
    touristInfoItem: null,
    showTouristInfo: false
  }

  componentWillMount = () => {
  }

  componentDidMount = () => {
    this.setState({showTouristInfo: true});
    fetch('/touristinfos.json').then((resp) => {
      return resp.json();
    }).then((resp) => {
      this.setState({TouristInfos: resp.touristinfos});
    });
  }

  showTouristInfoModal = (show, touristInfoItem) => {
    this.setState({showTouristInfoModal: show, touristInfoItem});
  }

  componentWillUnmount = () => {
  }

  render() {

    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      customPaging: (dots) => {
        return <React.Fragment>{dots}</React.Fragment>
      }
    };

    return (
      <div id="touristinfo">

        <div id="slideshow">
        <Slider {...settings} ref={c => this.slideshow = c}>
          <div>
            <img alt="" src="/images/slides/slide-01.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-02.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-03.jpg" />
          </div>
          <div>
            <img alt="" src="/images/slides/slide-04.jpg" />
          </div>
        </Slider>
        </div>

        <div id="touristinfo-controls">
          <Link to="/" className="back-button">
            <i className="fa fa-chevron-left"></i>Home
          </Link>
        </div>

      <div id="touristinfo-schedules">
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h1 className="page-title">Don't Miss...</h1>
              </div>
            </div>
          </div>
        </div>

      <div id="touristinfo-container">

        <div id="touristinfos">
          <div className="row">
            <div className="col">
              <div className="touristinfolist">
                <TransitionGroup>
                {this.state.TouristInfos.map((touristinfo, index) => {
                  
                  let delay = Math.max(0, index * 100);

                  if(this.state.showTouristInfo){
                    return (
                      <CSSTransition key={`touristinfo-${touristinfo.id}`} classNames="slideUp" timeout={3000 + delay} style={{transitionDelay: `${delay}ms`}}>
                        <a key={touristinfo.id} className={`btn card-tourist-info`} onClick={() => { this.showTouristInfoModal(true, touristinfo) }}>
                          <img className="card-img-left" alt="" style={{backgroundImage: `url("${touristinfo.image}")`}} />
                          <div className="card-container">
                            <h5 className="card-title">{touristinfo.name}</h5>
                            <p className="card-text">{touristinfo.description_excerpt}</p>
                          </div>
                        </a>
                      </CSSTransition>
                    )
                  }
                  return null;
                })}
                </TransitionGroup>
              </div>
            </div>
          </div>
        </div>

        </div>

        {this.state.showTouristInfoModal && 
          createPortal( <TouristInfoModal touristInfoItem={this.state.touristInfoItem} hideTouristInfoModal={ () => {this.showTouristInfoModal(false);}}></TouristInfoModal>, document.getElementById('modal-portal'))
        }

      </div>
    )
  }
}

export default TouristInfo;

