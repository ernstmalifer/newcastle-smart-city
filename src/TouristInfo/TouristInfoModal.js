import React, { Component } from 'react';

import QRCode from 'qrcode.react';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import './TouristInfoModal.css';

class TouristInfoModal extends Component {

  state = {
    willShow: false,
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({willShow: true})
    }, 100);
  }

  hideTouristInfoModal = () => {
    this.setState({willShow: false}, () => {
      setTimeout(() => {
        this.props.hideTouristInfoModal();
      }, 100);
    })
  }

  render() {

    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      customPaging: (dots) => {
        return <React.Fragment>{dots}</React.Fragment>
      }
    };


    return (
      <React.Fragment>
        <div className={`modal fade ${this.state.willShow ?  `show` : ``}`} role="dialog" style={{display: this.state.willShow ? `block` : ``}}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="row">
                  <div className="col">
                    <h5 className="modal-title tourist-info-modal-title">{this.props.touristInfoItem.name}</h5>
                  </div>
                  <div className="col">
                    <img className="tourist-info-image" alt="" style={{backgroundImage: `url("${this.props.touristInfoItem.image}")`}} />
                  </div>
                </div>

                <div className="row">
                <div className="col">
                    <div className="tourist-info-description" dangerouslySetInnerHTML={{__html: this.props.touristInfoItem.description}} />
                  </div>
                </div>
                
                <div className="row">
                  <div id="modal-slideshow">
                  <a id="slideshow-previousSlide" onClick={() => { this.slideshow.slickPrev(); }}><i className="fa fa-chevron-left"></i></a>
                  <a id="slideshow-nextSlide" onClick={() => { this.slideshow.slickNext(); }}><i className="fa fa-chevron-right"></i></a>
                  <Slider {...settings} ref={c => this.slideshow = c}>
                    <div>
                      <img alt="" src="/images/slides/slide-01.jpg" />
                    </div>
                    <div>
                      <img alt="" src="/images/slides/slide-02.jpg" />
                    </div>
                    <div>
                      <img alt="" src="/images/slides/slide-03.jpg" />
                    </div>
                    <div>
                      <img alt="" src="/images/slides/slide-04.jpg" />
                    </div>
                  </Slider>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <p className="tourist-info-qr">
                      <QRCode value={this.props.touristInfoItem.url} size={128} bgColor={"#ffffff"} fgColor={"#000000"} level={"L"}/>
                      <span className="qr-knowmore">Scan the QR code to know more!</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="tourist-info-close-btn btn btn-lg btn-secondary" data-dismiss="modal" onClick={() => {this.hideTouristInfoModal()}}>Close</button>
              </div>
            </div>
          </div>
        </div>
        <div className={`modal-backdrop fade ${this.state.willShow ?  `show` : `hide`}`}></div>
      </React.Fragment>
    );
  }
}

export default TouristInfoModal;