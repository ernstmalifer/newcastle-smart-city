/* eslint-disable no-undef */
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'weather-icons/css/weather-icons.css';
import './App.css';

import { history } from './store';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import Header from './components/Header';
import Footer from './components/Footer';

import Home from './Home/Home';
import Wayfinding from './Wayfinding/Wayfinding';
import Whatson from './Whatson/Whatson';
import LocalHistory from './LocalHistory/LocalHistory';
import LocalHistoryItem from './LocalHistory/LocalHistoryItem';
import TouristInfo from './TouristInfo/TouristInfo';

import AttractLoop from './AttractLoop/AttractLoop';

class App extends Component {

  componentDidMount = () => {
    this.toAttractLoop();
  }

  toAttractLoop = () => {
    this.toAttractLoopTimeout = setTimeout(() => {
      history.push('/attractloop')
    }, 60000)
  }

  onTouchEnd = () => {
    clearTimeout(this.toAttractLoopTimeout);
    this.toAttractLoop();
  }

  render() {

    return (
      <div className="App" onTouchEnd={() => this.onTouchEnd()}>
        <div id="modal-portal"></div>
        <Header />
        <Route render={({ location }) => (
          <TransitionGroup className="route-container">
            <CSSTransition key={location.key} classNames="fade" timeout={500}>
              <Switch location={location}>
                <Route exact path="/" component={Home} />
                <Route exact path="/attractloop" component={AttractLoop} />
                <Route exact path="/wayfinding" component={Wayfinding} />
                <Route exact path="/whatson" component={Whatson} />
                <Route exact path="/localhistory" component={LocalHistory} />
                <Route exact path="/localhistory/:item" component={LocalHistoryItem} />
                <Route exact path="/touristinfo" component={TouristInfo} />
                <Route render={() => <div>Not Found</div>} />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        )} />
        <Footer />
      </div>
    );
  }
}

export default App;
