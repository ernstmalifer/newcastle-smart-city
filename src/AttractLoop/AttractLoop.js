/* eslint-disable no-undef */
import React, { Component } from 'react';
import './AttractLoop.css';

import { Link } from 'react-router-dom';

class AttractLoop extends Component {

  state = {
  }

  componentWillMount = () => {
  }

  componentDidMount = () => {
  }

  componentWillUnmount = () => {
  }

  render() {

    return (
      <div id="attractloop">
        <Link to="/">
          <video autoPlay={true} controls={false} loop muted>
            <source src="videos/_ATTRACTLOOP_opt.mp4" type="video/mp4" />
          </video>
        </Link>
      </div>
    )
  }
}

export default AttractLoop;

