import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './App';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from './store';
// import store from './store';

import registerServiceWorker from './registerServiceWorker';

// import { createHashHistory } from 'history';
// const history = createHashHistory({
//   hashType: 'slash',
//   getUserConfirmation: (message, callback) => callback(window.confirm(message))
// })

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
