This project was bootstrapped with [Create React App]

# Newcastle Smart City

Install Dependencies & Dev Dependencies
`npm install` or `yarn install`

Start Dev Server
`npm run start` or `yarn start`

Building
`npm run build` or `yarn build`